#include <AccelStepper.h>

class StepperMotor{
    public:
        AccelStepper stepper;
        StepperMotor(double ratio, uint16_t spr, uint8_t interface, uint8_t pin1, uint8_t pin2){
            _ratio = ratio;
            _spr = spr;
            stepper = AccelStepper(interface, pin1, pin2);
        }

        /**************************************************************************/
        /*!
            @brief    Convert azimuth degrees to steps according to step/revolution, rotator
                      gear box ratio and microstep
            @param    deg
                      Degrees in float format
            @return   Steps for stepper motor driver, int32_t
        */
        /**************************************************************************/
        int32_t deg2step(float deg) {
            return (_ratio * _spr * deg / 360);
        }
        
        /**************************************************************************/
        /*!
            @brief    Convert azimuth steps to degrees according to step/revolution, rotator
                      gear box ratio and microstep
            @param    step
                      Steps in int32_t format
            @return   Degrees in float format
        */
        /**************************************************************************/
        float step2deg(int32_t step) {
            return (360.00 * step / (_spr * _ratio));
        }
    private:
        double _ratio;
        uint16_t _spr;
};
