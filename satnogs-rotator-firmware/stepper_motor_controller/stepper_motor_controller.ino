/*!
 * @file stepper_motor_controller.ino
 *
 * This is the documentation for satnogs rotator controller firmware
 * for stepper motors configuration. The board (PCB) is placed in
 * <a href="https://gitlab.com/librespacefoundation/satnogs/satnogs-rotator-controller">
 * satnogs-rotator-controller </a> and is for releases:
 * v2.0
 * v2.1
 * v2.2
 * <a href="https://wiki.satnogs.org/SatNOGS_Rotator_Controller"> wiki page </a>
 *
 * @section dependencies Dependencies
 *
 * This firmware depends on <a href="http://www.airspayce.com/mikem/arduino/AccelStepper/index.htmlhttp://www.airspayce.com/mikem/arduino/AccelStepper/index.html">
 * AccelStepper library</a> being present on your system. Please make sure you
 * have installed the latest version before using this firmware.
 *
 * @section license License
 *
 * Licensed under the GPLv3.
 *
 */

#define SAMPLE_TIME        0.1   ///< Control loop in s
#define M1_RATIO           3.375    ///< Gear ratio of azimuth gear box
#define M2_RATIO           12    ///< Gear ratio of elevation gear box
//#define MICROSTEP          8     ///< Set Microstep (unused)
#define MIN_PULSE_WIDTH    20    ///< In microsecond for AccelStepper
#define MAX_M1_SPEED          600  ///< Max azimuth speed In steps/s, consider the microstep
#define MAX_M1_ACCELERATION   400  ///< Max azimuth accel In steps/s^2, consider the microstep
#define MAX_M2_SPEED          1050  ///< Max azimuth speed In steps/s, consider the microstep
#define MAX_M2_ACCELERATION   700  ///< Max azimuth accel In steps/s^2, consider the microstep
#define M1_SPR                3200L ///< Step Per Revolution, consider the microstep
#define M2_SPR                1600L
#define MIN_M1_ANGLE       0     ///< Minimum angle of azimuth
#define MAX_M1_ANGLE       360   ///< Maximum angle of azimuth
#define MIN_M2_ANGLE       0     ///< Minimum angle of elevation
#define MAX_M2_ANGLE       180   ///< Maximum angle of elevation
#define DEFAULT_HOME_STATE HIGH  ///< Change to LOW according to Home sensor
#define HOME_DELAY         12000 ///< Time for homing Deceleration in millisecond

#include <AccelStepper.h>
#include <Wire.h>
#include "libraries/stepper_motor.h"
#include "libraries/globals.h"
#include "libraries/easycomm.h"
#include "libraries/rotator_pins.h"
#include "libraries/rs485.h"
#include "libraries/endstop.h"
#include "libraries/watchdog.h"

uint32_t t_run = 0; // run time of uC
easycomm comm;
StepperMotor az(M1_RATIO, M1_SPR, 1, M1IN1, M1IN2);
StepperMotor el(M2_RATIO, M2_SPR, 1, M2IN1, M2IN2);
endstop switch_az(SW1, DEFAULT_HOME_STATE), switch_el(SW2, DEFAULT_HOME_STATE);
wdt_timer wdt;

enum _rotator_error homing(int32_t seek_az, int32_t seek_el);
int32_t deg2step(float deg);
float step2deg(int32_t step);

void setup() {
    // Homing switch
    switch_az.init();
    //switch_el.init();

    // Serial Communication
    comm.easycomm_init();

    // Stepper Motor setup
    az.stepper.setEnablePin(MOTOR_EN);
    az.stepper.setPinsInverted(false, false, true);
    az.stepper.enableOutputs();
    az.stepper.setMaxSpeed(MAX_M1_SPEED);
    az.stepper.setAcceleration(MAX_M1_ACCELERATION);
    az.stepper.setMinPulseWidth(MIN_PULSE_WIDTH);
    el.stepper.setPinsInverted(false, false, true);
    el.stepper.enableOutputs();
    el.stepper.setMaxSpeed(MAX_M2_SPEED);
    el.stepper.setAcceleration(MAX_M2_ACCELERATION);
    el.stepper.setMinPulseWidth(MIN_PULSE_WIDTH);

    // Initialize WDT
    wdt.watchdog_init();
}

void loop() {
    // Update WDT
    wdt.watchdog_reset();

    // Get end stop status
    rotator.switch_az = switch_az.get_state();
    rotator.switch_el = switch_el.get_state();

    // Run easycomm implementation
    comm.easycomm_proc();

    // Get position of both axis
    control_az.input = az.step2deg(az.stepper.currentPosition());
    control_el.input = el.step2deg(el.stepper.currentPosition());

    // Check rotator status
    if (rotator.rotator_status != error) {
        if (rotator.homing_flag == false) {
            // Check home flag
            rotator.control_mode = position;
            // Homing
            rotator.rotator_error = homing(az.deg2step(-MAX_M1_ANGLE),
                                           el.deg2step(-MAX_M2_ANGLE));
            if (rotator.rotator_error == no_error) {
                // No error
                rotator.rotator_status = idle;
                rotator.homing_flag = true;
            } else {
                // Error
                rotator.rotator_status = error;
                rotator.rotator_error = homing_error;
            }
        } else {
            // Control Loop
            az.stepper.moveTo(az.deg2step(control_az.setpoint));
            el.stepper.moveTo(el.deg2step(control_el.setpoint));
            rotator.rotator_status = pointing;
            // Move azimuth and elevation motors
            az.stepper.run();
            el.stepper.run();
            // Idle rotator
            if (az.stepper.distanceToGo() == 0 && el.stepper.distanceToGo() == 0) {
                rotator.rotator_status = idle;
            }
        }
    } else {
        // Error handler, stop motors and disable the motor driver
        az.stepper.stop();
        az.stepper.disableOutputs();
        el.stepper.stop();
        el.stepper.disableOutputs();
        if (rotator.rotator_error != homing_error) {
            // Reset error according to error value
            rotator.rotator_error = no_error;
            rotator.rotator_status = idle;
        }
    }
}

/**************************************************************************/
/*!
    @brief    Move both axis with one direction in order to find home position,
              end-stop switches
    @param    seek_az
              Steps to find home position for azimuth axis
    @param    seek_el
              Steps to find home position for elevation axis
    @return   _rotator_error
*/
/**************************************************************************/
enum _rotator_error homing(int32_t seek_az, int32_t seek_el) {
    bool isHome_az = false;
    bool isHome_el = false;

    // Move motors to "seek" position
    az.stepper.moveTo(seek_az);
    el.stepper.moveTo(seek_el);

    // Homing loop
    while (isHome_az == false || isHome_el == false) {
        // Update WDT
        wdt.watchdog_reset();
        if (switch_az.get_state() == true && !isHome_az) {
            // Find azimuth home
            az.stepper.moveTo(az.stepper.currentPosition());
            isHome_az = true;
        }
        if (switch_el.get_state() == true && !isHome_el) {
            // Find elevation home
            el.stepper.moveTo(el.stepper.currentPosition());
            isHome_el = true;
        }
        // Check if the rotator goes out of limits or something goes wrong (in
        // mechanical)
        if ((az.stepper.distanceToGo() == 0 && !isHome_az) ||
            (el.stepper.distanceToGo() == 0 && !isHome_el)){
            return homing_error;
        }
        // Move motors to "seek" position
        az.stepper.run();
        el.stepper.run();
    }
    // Delay to Deccelerate and homing, to complete the movements
    uint32_t time = millis();
    while (millis() - time < HOME_DELAY) {
        wdt.watchdog_reset();
        az.stepper.run();
        el.stepper.run();
    }
    // Set the home position and reset all critical control variables
    az.stepper.setCurrentPosition(0);
    el.stepper.setCurrentPosition(0);
    control_az.setpoint = 0;
    control_el.setpoint = 0;

    return no_error;
}
